<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-4' ); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item footer-copyright">
					<?php dynamic_sidebar( 'footer-copyright' ); ?>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item footer-social">
					<?php dynamic_sidebar( 'footer-social' ); ?>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item footer-category">
					<?php dynamic_sidebar( 'footer-taxonomy' ); ?>
				</div>
			</div>
		</div>
	</div>
</footer>

<div id="back-to-top">
    <a href="javascript:void(0)"></a>
</div>

<?php wp_footer(); ?>
</body>
</html>