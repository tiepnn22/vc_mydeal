<?php
class support extends WP_Widget {
    function __construct() {
        parent::__construct(
            'support',
            'support',
            array( 'description'  =>  'Tiệp - support' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'meta' => '',
            'phone' => '',
            'linkphone' => '',
            'email' => '',
            'linkemail' => '',
            'title2' => '',
            'address' => '',
            'timework' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $meta = esc_attr($instance['meta']);
        $phone = esc_attr($instance['phone']);
        $linkphone = esc_attr($instance['linkphone']);
        $email = esc_attr($instance['email']);
        $linkemail = esc_attr($instance['linkemail']);
        $title2 = esc_attr($instance['title2']);
        $address = esc_attr($instance['address']);
        $timework = esc_attr($instance['timework']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Mô tả:<input type="text" class="widefat" name="'.$this->get_field_name('meta').'" value="'.$meta.'"/></p>';
        echo '<p>Phone:<input type="text" class="widefat" name="'.$this->get_field_name('phone').'" value="'.$phone.'"/></p>';
        echo '<p>Link phone:<input type="text" class="widefat" name="'.$this->get_field_name('linkphone').'" value="'.$linkphone.'"/></p>';
        echo '<p>Email:<input type="text" class="widefat" name="'.$this->get_field_name('email').'" value="'.$email.'"/></p>';
        echo '<p>Link email:<input type="text" class="widefat" name="'.$this->get_field_name('linkemail').'" value="'.$linkemail.'"/></p>';
        echo '<p>Tiêu đề 2:<input type="text" class="widefat" name="'.$this->get_field_name('title2').'" value="'.$title2.'"/></p>';
        echo '<p>Địa chỉ:<input type="text" class="widefat" name="'.$this->get_field_name('address').'" value="'.$address.'"/></p>';
        echo '<p>Thời gian làm việc:<input type="text" class="widefat" name="'.$this->get_field_name('timework').'" value="'.$timework.'"/></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['meta'] = ($new_instance['meta']);
        $instance['phone'] = ($new_instance['phone']);
        $instance['linkphone'] = ($new_instance['linkphone']);
        $instance['email'] = ($new_instance['email']);
        $instance['linkemail'] = ($new_instance['linkemail']);
        $instance['title2'] = ($new_instance['title2']);
        $instance['address'] = ($new_instance['address']);
        $instance['timework'] = ($new_instance['timework']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $meta = $instance['meta'];
        $phone = $instance['phone'];
        $linkphone = $instance['linkphone'];
        $email = $instance['email'];
        $linkemail = $instance['linkemail'];
        $title2 = $instance['title2'];
        $address = $instance['address'];
        $timework = $instance['timework'];

        echo $before_widget;
            echo $before_title.$title.$after_title;
            echo '<span>'.$meta.'</span>';
            echo '<a class="tell" href="tell:'.$linkphone.'"><i class="fa fa-mobile" aria-hidden="true"></i>'.$phone.'</a>';
            echo '<a class="mail" href="mailto:'.$linkemail.'"><i class="fa fa-envelope-o" aria-hidden="true"></i>'.$email.'</a>';
            echo '<span class="title2">'.$title2.'</span>';
            echo '<span>'.$address.'</span>';
            echo '<span>'.$timework.'</span>';
        echo $after_widget;
    }
}
function create_support_widget() {
    register_widget('support');
}
add_action( 'widgets_init', 'create_support_widget' );
?>