<?php
class img extends WP_Widget {
    function __construct() {
        parent::__construct(
            'img',
            'ads',
            array( 'description'  =>  'Tiệp - ads' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'img' => '',
            'url' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $img = esc_attr($instance['img']);
        $url = esc_attr($instance['url']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>URL ảnh:<input type="text" class="widefat" name="'.$this->get_field_name('img').'" value="'.$img.'" /></p>';
        echo '<p>Link ảnh:<input type="text" class="widefat" name="'.$this->get_field_name('url').'" value="'.$url.'" /></p>';
        echo '<img style="max-width:100%;" src="'.$img.'" />';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['img'] = ($new_instance['img']);
        $instance['url'] = ($new_instance['url']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $img = $instance['img'];
        $url = $instance['url'];

        echo $before_widget;
            if( !empty($img) ) {
                echo '<a href="'.$url.'" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ads-home"><img class="img-responsive" src="'.$img. '"></a>';
            }
        echo $after_widget;
    }
}
function create_img_widget() {
    register_widget('img');
}
add_action( 'widgets_init', 'create_img_widget' );
?>