<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
			'post_type' => 'product',
			'tax_query' => array(
								array(
										'taxonomy' => 'product_cat',
										'field' => 'id',
										'terms' => $term_ids,
										'operator'=> 'IN'
								 )),
			'posts_per_page' => 4,
			'orderby' => 'date',
			'post__not_in'=>array($post->ID)
	 ) );
?>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 product-related">
	<?php dynamic_sidebar( 'support' ); ?>
	
	<div class="related-title">
		<a>
			<h2>Deal liên quan</h2>
		</a>
	</div>
	
	<div class="related-content">
	<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
		<article class="item">
    		<div class="shadow p-info">
				<figure>
					<a href="<?php the_permalink();?>">
						<img class="img-responsive" src="<?php echo tiep_get_thumbnail_url('product') ?>" alt="<?php the_title();?>" />
					</a>
				</figure>
				
				<div class="p-detail">

					<div class="p-title">
						<a href="<?php the_permalink();?>">
							<h3>
								<?php the_title();?>									
							</h3>
						</a>
					</div>
					
					<?php get_template_part("resources/template/template-show-price"); ?>
					
					<a class="read-more" href="<?php the_permalink();?>">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</a>
					
					<div class="p-view">
						<span class="p-view-count"><?php echo show_count_price_product(); ?></span>
						<i class="fa fa-user" aria-hidden="true"></i>
					</div>

				</div>

			</div>
		</article>
	<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
	</div>
</div>